/*
create table anime
(
 id INTEGER  PRIMARY KEY  AUTOINCREMENT,
 name text(500) not null,
 genre text(250) not null,
 type text(250) null
	
);
*/

select * from anime;

--insert into anime (name,genre,type) values ('Prison School','Gore','SERIES');
insert into anime (name,genre,type) values ('Elfen Lied','Gore','SERIES');
insert into anime (name,genre,type) values ('Higurashi no naku koro ni','Gore','SERIES');
insert into anime (name,genre,type) values ('Umineko no naku koro ni','Gore','SERIES');
insert into anime (name,genre,type) values ('Shy','Shonen','SERIES');

insert into anime (name,genre,type) values ('Boku no hero academia','Shonen','SERIES');
insert into anime (name,genre,type) values ('DR STONE','Science','SERIES');
insert into anime (name,genre,type) values ('Sousou no Frieren','Fantasy','SERIES');
insert into anime (name,genre,type) values ('Naruto','Shonen','SERIES');



insert into anime (name,genre,type) values ('Suzume no tojimari','Fantasy','FILM');
insert into anime (name,genre,type) values ('Tenki no Ko','Fantasy','FILM');
insert into anime (name,genre,type) values ('Kimi no nawa','Fantasy','FILM');



