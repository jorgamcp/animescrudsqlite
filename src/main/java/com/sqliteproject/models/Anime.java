package com.sqliteproject.models;

public class Anime {
	
	private int id;
	private String name;
	private String genre;
	private String type;
	
	public Anime(int id,String name,String genre,String type)
	{
		this.id=id;
		this.name=name;
		this.genre=genre;
		this.type=type;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	public int getId()
	{
		return this.id;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getName()
	{
		return this.name;
	}
	public void setGenre(String genre)
	{
		this.genre = genre;
	}
	public String getGenre()
	{
		return this.genre;
	}
	public void setType(String type)
	{
		this.type = type;
	}
	public String getType()
	{
		return this.type;
	}
	
	@Override
	public String toString()
	{
		return "Anime [ Id " + this.id + " ]\nName: "
	+this.name+"\nGenre: "
				+this.genre+"\n"+
	"Type: "+this.type+"\n\n";
	}
}
