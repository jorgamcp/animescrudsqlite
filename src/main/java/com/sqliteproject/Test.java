package com.sqliteproject;

import java.sql.SQLException;
import java.util.List;

import com.sqliteproject.DAO.AnimeDAO;
import com.sqliteproject.models.Anime;

public class Test {
	static AnimeDAO animeDao;

	
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub

		System.out.println("CRUD Anime SQLite Powered");
		System.out.println("=============================");
		System.out.println("Enter an option:");

		read();

	}

	static void read() throws SQLException {
		animeDao = new AnimeDAO();
		List<Anime> animes = animeDao.read();

		for (Anime a : animes) {
			System.out.println(a.toString());
		}
	}
	static void create() throws SQLException
	{
		animeDao = new AnimeDAO();
		animeDao.create();
	}
	static void update() throws SQLException
	{
		animeDao = new AnimeDAO();
		animeDao.update();
	}
}
