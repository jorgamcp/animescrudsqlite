package com.sqliteproject.DAO;

import java.util.List;

public interface DAO<E> {
	void create();
	List<E> read();
	void update();
	void delete();
}
