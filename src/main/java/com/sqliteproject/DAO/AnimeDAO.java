package com.sqliteproject.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.sql.ResultSet;

import com.sqliteproject.models.Anime;

public class AnimeDAO implements DAO<Anime> {

	private Anime anime;
	private ArrayList<Anime> listAnime;

	private Connection conn;

	private final String SQLITEDB_FILE_NAME = "sample.db";
	private final String JDBC_URL = "jdbc:sqlite:" + SQLITEDB_FILE_NAME;
	private Statement statement;

	private final String SQL_READ_QUERY = "SELECT id,name,genre,type FROM anime;";
	private final String SQL_INSERT_ANIME = "INSERT INTO anime(name,genre,type) VALUES (?,?,?)";
	private final String SQL_GET_ONE_ANIME ="SELECT id,name,genre,type FROM anime WHERE id = ?";
	
	Scanner scanner = new Scanner(System.in);
	public AnimeDAO() throws SQLException {
		conn = DriverManager.getConnection(JDBC_URL);
		listAnime = new ArrayList<Anime>();

	}

	@Override
	public void create() {
		// TODO Auto-generated method stub
		
		String name,genre,type;
	
		
		System.out.println("INSERT Anime:");
		System.out.println("Name:\t");
		name = scanner.next();
		System.out.println("Genre:\t");
		genre = scanner.next();
		System.out.println("Type:\t");
		type=scanner.next();
		
		scanner.close();
		Anime anime = new Anime(0,name,genre,type);
		
		System.out.println("Inserting anime ["+anime.getName()+"]");
		
		try
		{
			PreparedStatement pstatement = 
					conn.prepareStatement(SQL_INSERT_ANIME,Statement.RETURN_GENERATED_KEYS);
			
			pstatement.setString(1, name);
			pstatement.setString(2, genre);
			pstatement.setString(3, type);
			
			
			pstatement.executeUpdate();
			
			int generatedId = 0;
			ResultSet rs = pstatement.getGeneratedKeys();
			if(rs.next())
			{
				generatedId = rs.getInt(1);
				anime.setId(generatedId);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		finally {
			System.out.println("Anime " + anime.getName() + " was inserted");
			System.out.println("With Id " + anime.getId());
		}
	}

	@Override
	public List<Anime> read() {
		// TODO Auto-generated method stub

		try {
			statement = conn.createStatement();
			 
			ResultSet result_set = statement.executeQuery(SQL_READ_QUERY);

			while (result_set.next()) {
				int id = result_set.getInt("id");
				String name = result_set.getString("name");
				String genre = result_set.getString("genre");
				String type = result_set.getString("type");
				
				anime = new Anime(id,name,genre,type);
				
				listAnime.add(anime);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listAnime;

	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
		System.out.println("Anime Update");
		System.out.println("Which anime do you want to update?\n\tProvide the Id:");
		int animeid = scanner.nextInt();
		
		try
		{
			PreparedStatement pstatement = 
					conn.prepareStatement(SQL_GET_ONE_ANIME);
			pstatement.setInt(1, animeid);
			
			ResultSet rs = pstatement.executeQuery();
			
			if(rs.next())
			{
				System.out.println();
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String genre = rs.getString("genre");
				String type = rs.getString("type");
				
				System.out.println("Anime with id "+ animeid + " Found " + name);
				anime = new Anime(id,name,genre,type);
				
				System.out.println(anime.toString());
			}
			else {
				System.out.println("Anime with id " + animeid + " was not found.");
			}
		}catch(SQLException ex)
		{
			ex.printStackTrace();
		}
		
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub

	}

}
